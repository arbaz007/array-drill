function map(elements, cb) {
    // Do NOT use .map, to complete this function.
    // How map works: Map calls a provided callback function once for each element in an array, in order, and functionructs a new array from the res .
    // Produces a new array of values by mapping each value in list through a transformation function (iteratee).
    // Return the new array.

    if (Array.isArray(elements) || elements.length > 0) {
        let result = [];
        for (let index = 0; index < elements.length; index++) {
            if (typeof elements[index] != "undefined"){
                result.push(cb(elements[index], index))
            }
        }
        return result
    }else {
        console.log("first argument is not an array");
    }

}

let arr = [1,2,5,8]

let s = map(arr, (each, index) => {
    return each*2
})


module.exports = map
