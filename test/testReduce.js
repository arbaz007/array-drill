const reduce = require("../reduce");
const items = [1, 2, 3, 4, 5, 5]; // use this array to test your code.
let result = reduce(items, (acc, each, index) => {
    acc.push(each*2)
    return acc
}, [])
console.log(result);