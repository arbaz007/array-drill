function filter(elements, cb) {
  // Do NOT use .filter, to complete this function.
  // Similar to `find` but you will return an array of all elements that passed the truth test
  // Return an empty array if no elements pass the truth test
  let ans = [];
  if (Array.isArray(elements)) {
    if (elements.length > 0) {
      for (index in elements) {
        if (cb(elements[index], index)) {
          ans.push(elements[index]);
        }
      }
    }
  }
  return ans
}



module.exports = filter