function flatten(elements, ans = []) {

  // Flattens a nested array (the nesting can be to any depth).
  // Hint: You can solve this using recursion.
  // Example: flatten([1, [2], [3, [[4]]]]); => [1, 2, 3, 4];

  if (Array.isArray(elements)) {
    if (elements.length > 0) {
      for (row of elements) {
        if (Array.isArray(row)) {
          flatten(row, ans)
        } else {
          ans.push(row);
        }
      }
    }
  }
  return ans;

}



module.exports = flatten
