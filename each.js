function each(elements, cb) {
    // Do NOT use forEach to complete this function.
    // Iterates over a list of elements, yielding each in turn to the `cb` function.
    // This only needs to work with arrays.
    // You should also pass the index into `cb` as the second argument
    // based off http://underscorejs.org/#each

    if (Array.isArray(elements) || elements.length > 0) {
        for (let index = 0; index < elements.length; index++) {
            if (typeof elements[index] !== "undefined"){
                cb (elements[index], index)
            }
        }
    }else {
        console.log("first argument is not an array");
    }
}

module.exports = each